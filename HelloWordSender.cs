﻿using System;
using System.Threading;
using HelloWordSender.Service.HelloWordService;
using HelloWordSender.Service.QueueBizService;
using HelloWordSender.Service.QueueService;
using HelloWordSender.Service.QueueService.Interfaces;
using HelloWordSender.Service.TokenService;

namespace HellowordSender
{
    class Program
    {
        static void Main(string[] args)
        {
            TokenBizService tokenService = new TokenBizService();
            SendMessageBizService sendMessage = new SendMessageBizService(tokenService);
            QueueBizService queueService = new QueueBizService(sendMessage);

            while(true)
            {
                queueService.SendMessageToQueue();
                queueService.ReceiveMensageQueue();
            }
        }
    }
}
