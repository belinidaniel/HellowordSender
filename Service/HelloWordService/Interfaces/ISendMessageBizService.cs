using System;
using HelloWordSender.Model;

namespace HelloWordSender.Service.TokenService.Interface
{
    public interface ISendMessageBizService
    {
        Message BuildMessageToSend(string message, DateTime time);
    }
}