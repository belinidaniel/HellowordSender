using System;
using HelloWordSender.Model;
using HelloWordSender.Service.TokenService.Interface;

namespace HelloWordSender.Service.HelloWordService
{
    public class SendMessageBizService : ISendMessageBizService
    {   
        private readonly IToKenBizService _tokenBizService;

        public SendMessageBizService(IToKenBizService tokenService)
        {
            _tokenBizService = tokenService;
        }


        public Message BuildMessageToSend(string message, DateTime time)
        {
            Message msg = new Message();
            try{
                if((message != null || message != "") && time != null)
                {
                    msg.MessageToSend = message;
                    //formatando o tempo atual em string 
                    msg.timestamp = time.ToString("yyyyMMddHHmmssffff");
                    //gerando o token da transacao
                    msg.Token = _tokenBizService.RandomPassword();
                    return msg;
                }
            }catch(Exception e){
                throw new Exception("erro ao criar a messagem  : " + e);
            }
            return msg;
        }
    }

}