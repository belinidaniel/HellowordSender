using System;
using RabbitMQ.Client;
using System.Text;
using System.Threading;
using HelloWordSender.Service.TokenService.Interface;
using HelloWordSender.Model;
using RabbitMQ.Client.Events;
using HelloWordSender.Dto;
using  HelloWordSender.Service.QueueService.Interfaces;

namespace HelloWordSender.Service.QueueBizService
{
    public class QueueBizService : IQueueBizService
    {
        
        private readonly ISendMessageBizService _sendMessageBizService;
        private static string queue = "helloWordQueue"; 
        private static string host = "localhost";
        
        public QueueBizService(ISendMessageBizService sendMessageBizService)
        {
            _sendMessageBizService = sendMessageBizService;
        }

        //Methodo que vai fazer a chamada para o Rabbit e mandar a mensagem        
        public void SendMessageToQueue()
        {
            var factory = new ConnectionFactory() { HostName = host };
            using(var connection = factory.CreateConnection())
            using(var channel = connection.CreateModel())
            {
                try
                {
                    channel.QueueDeclare(queue: queue,
                                        durable: false,
                                        exclusive: false,
                                        autoDelete: false,
                                        arguments: null);
                        //criando a mensagem
                        Message msg = _sendMessageBizService.BuildMessageToSend("Hello World!", DateTime.Now);
                        
                        //gerando a mensangem DTO que vai ser enviado para a queue
                        MessageDTO message = new MessageDTO(msg);
                        var body = Encoding.UTF8.GetBytes(message.MessageToQueue);

                        channel.BasicPublish(exchange: "",
                                            routingKey: queue,
                                            basicProperties: null,
                                            body: body);

                        Console.WriteLine(message.MessageToQueue);
                Thread.Sleep(5000);

                }catch(Exception e)
                {
                    throw new System.Exception("Falha ao Enviar a mensagem para a queue" + e);
                }
                
            }
        }
    
        public void ReceiveMensageQueue()
        {
            var factory = new ConnectionFactory() { HostName = host };
            using(var connection = factory.CreateConnection())
            using(var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue,
                                    durable: false,
                                    exclusive: false,
                                    autoDelete: false,
                                    arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received {0}", message);
                };
                channel.BasicConsume(queue: queue,
                                    autoAck: true,
                                    consumer: consumer);
            }
        }
    }
}