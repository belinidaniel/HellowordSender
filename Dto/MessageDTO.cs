using HelloWordSender.Model;

namespace HelloWordSender.Dto
{
    public class MessageDTO
    {
        public MessageDTO(Message message){
            this.MessageToQueue = "Message : "+ message.MessageToSend +", Execution Time : " + message.timestamp + ", Tokem:" + message.Token;
        }
        public string MessageToQueue{get;set;}
    }
}