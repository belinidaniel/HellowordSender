namespace HelloWordSender.Model
{
    using System;

    public class Message
    {

        public string MessageToSend{get; set;}

        public string timestamp {get; set;}

        public string Token {get ; set;}            
    }
}